﻿using System;
using Be.IO;
using NBTParser.NBT.Tag;

namespace NBTParser.NBT
{
    public static class Parser
    {
        private static readonly Type[] _tagTypes =
        {
            typeof(EndTag), typeof(ByteTag), typeof(ShortTag), typeof(IntTag), typeof(LongTag), typeof(FloatTag),
            typeof(DoubleTag), typeof(ByteArrayTag), typeof(StringTag), typeof(ListTag), typeof(CompoundTag),
            typeof(IntArrayTag), typeof(LongArrayTag)
        };

        public static BaseTag ParseTag(BeBinaryReader beBinaryReader)
        {
            var tagId = beBinaryReader.ReadByte();

            var tag = (BaseTag) Activator.CreateInstance(_tagTypes[tagId]);

            tag.Id = tagId;

            tag.HandleData(beBinaryReader);

            return tag;
        }

        public static BaseTag ParseTagList(BeBinaryReader beBinaryReader, byte tagId)
        {
            var tag = (BaseTag) Activator.CreateInstance(_tagTypes[tagId]);

            tag.Id = tagId;

            tag.HandleDataList(beBinaryReader);

            return tag;
        }
    }
}