﻿using Be.IO;

namespace NBTParser.NBT.Tag
{
    public class DoubleTag : BaseTag
    {
        public double D;

        public DoubleTag(string name, double d)
        {
            D = d;
            Name = name;
            Id = 6;
        }

        public DoubleTag(double d)
        {
            D = d;
            Id = 6;
        }
        
        public DoubleTag()
        {
        }

        public override object Data()
        {
            return D;
        }

        public override void HandleData(BeBinaryReader beBinaryReader)
        {
            _loadName(beBinaryReader);

            D = beBinaryReader.ReadDouble();
        }

        public override void HandleDataList(BeBinaryReader beBinaryReader)
        {
            D = beBinaryReader.ReadDouble();
        }

        public override void Save(BeBinaryWriter beBinaryWriter)
        {
            _saveId(beBinaryWriter);
            _saveName(beBinaryWriter);

            beBinaryWriter.Write(D);
        }

        public override void SaveList(BeBinaryWriter beBinaryWriter)
        {
            beBinaryWriter.Write(D);
        }

        public override string ToString()
        {
            return Name + ": " + D;
        }

        public override string ToStringList()
        {
            return D.ToString();
        }

        protected bool Equals(DoubleTag other)
        {
            return D.Equals(other.D);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((DoubleTag) obj);
        }

        public override int GetHashCode()
        {
            return D.GetHashCode();
        }
    }
}