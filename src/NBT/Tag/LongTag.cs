﻿using Be.IO;

namespace NBTParser.NBT.Tag
{
    public class LongTag : BaseTag
    {
        public long L;

        public LongTag(string name, long l)
        {
            Name = name;
            L = l;
            Id = 4;
        }
        
        public LongTag(long l)
        {
            L = l;
            Id = 4;
        }

        public LongTag()
        {
        }

        public override object Data()
        {
            return L;
        }

        public override void HandleData(BeBinaryReader beBinaryReader)
        {
            _loadName(beBinaryReader);

            L = beBinaryReader.ReadInt64();
        }

        public override void HandleDataList(BeBinaryReader beBinaryReader)
        {
            L = beBinaryReader.ReadInt64();
        }

        public override void Save(BeBinaryWriter beBinaryWriter)
        {
            _saveId(beBinaryWriter);
            _saveName(beBinaryWriter);

            beBinaryWriter.Write(L);
        }

        public override void SaveList(BeBinaryWriter beBinaryWriter)
        {
            beBinaryWriter.Write(L);
        }

        public override string ToString()
        {
            return Name + ": " + L;
        }

        public override string ToStringList()
        {
            return L.ToString();
        }

        protected bool Equals(LongTag other)
        {
            return L == other.L;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((LongTag) obj);
        }

        public override int GetHashCode()
        {
            return L.GetHashCode();
        }
    }
}