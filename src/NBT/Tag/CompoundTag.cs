﻿using System.Collections.Generic;
using Be.IO;

namespace NBTParser.NBT.Tag
{
    public class CompoundTag : BaseTag
    {
        public List<BaseTag> Tags;

        public CompoundTag(string name, IEnumerable<BaseTag> tags)
        {
            Id = 10;

            Name = name;
            Tags = new List<BaseTag>(tags);
        }

        public CompoundTag(IEnumerable<BaseTag> tags)
        {
            Id = 10;

            Tags = new List<BaseTag>(tags);
        }
        
        public CompoundTag()
        {
            Tags = new List<BaseTag>();
        }

        public override object Data()
        {
            return Tags;
        }

        public override void HandleData(BeBinaryReader beBinaryReader)
        {
            _loadName(beBinaryReader);

            BaseTag tag;
            while (true)
            {
                tag = Parser.ParseTag(beBinaryReader);

                if (tag.GetType() != typeof(EndTag))
                    Tags.Add(tag);
                else
                    break;
            }
        }

        public override void HandleDataList(BeBinaryReader beBinaryReader)
        {
            BaseTag tag;
            while (true)
            {
                tag = Parser.ParseTag(beBinaryReader);

                if (tag.GetType() != typeof(EndTag))
                    Tags.Add(tag);
                else
                    break;
            }
        }

        public override void Save(BeBinaryWriter beBinaryWriter)
        {
            _saveId(beBinaryWriter);
            _saveName(beBinaryWriter);

            foreach (var nbtTag in Tags) nbtTag.Save(beBinaryWriter);

            new EndTag().Save(beBinaryWriter);
        }

        public override void SaveList(BeBinaryWriter beBinaryWriter)
        {
            foreach (var nbtTag in Tags) nbtTag.Save(beBinaryWriter);

            new EndTag().Save(beBinaryWriter);
        }

        public override string ToString()
        {
            var tags = string.Empty;

            foreach (var tag in Tags) tags += tag + ", ";

            tags = tags.Remove(tags.Length - 1);

            return Name + ": " + "{" + tags + "}";
        }

        public override string ToStringList()
        {
            var tags = string.Empty;

            foreach (var tag in Tags) tags += tag + ", ";

            tags = tags.TrimEnd().TrimEnd();

            return "{" + tags + "}";
        }

        protected bool Equals(CompoundTag other)
        {
            return Equals(Tags, other.Tags);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((CompoundTag) obj);
        }

        public override int GetHashCode()
        {
            return Tags != null ? Tags.GetHashCode() : 0;
        }
    }
}