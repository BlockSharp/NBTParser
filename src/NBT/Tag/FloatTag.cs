﻿using Be.IO;

namespace NBTParser.NBT.Tag
{
    public class FloatTag : BaseTag
    {
        public float F;

        public FloatTag(string name, float f)
        {
            Name = name;
            F = f;
            Id = 5;
        }
        
        public FloatTag(float f)
        {
            F = f;
            Id = 5;
        }

        public FloatTag()
        {
        }

        public override object Data()
        {
            return F;
        }

        public override void HandleData(BeBinaryReader beBinaryReader)
        {
            _loadName(beBinaryReader);

            F = beBinaryReader.ReadSingle();
        }

        public override void HandleDataList(BeBinaryReader beBinaryReader)
        {
            F = beBinaryReader.ReadSingle();
        }

        public override void Save(BeBinaryWriter beBinaryWriter)
        {
            _saveId(beBinaryWriter);
            _saveName(beBinaryWriter);

            beBinaryWriter.Write(F);
        }

        public override void SaveList(BeBinaryWriter beBinaryWriter)
        {
            beBinaryWriter.Write(F);
        }

        public override string ToString()
        {
            return Name + ": " + F;
        }

        public override string ToStringList()
        {
            return F.ToString();
        }

        protected bool Equals(FloatTag other)
        {
            return F.Equals(other.F);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((FloatTag) obj);
        }

        public override int GetHashCode()
        {
            return F.GetHashCode();
        }
    }
}