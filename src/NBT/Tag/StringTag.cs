﻿using Be.IO;

namespace NBTParser.NBT.Tag
{
    public class StringTag : BaseTag
    {
        public string Str;

        public StringTag(string name, string str)
        {
            Name = name;
            Str = str;
            Id = 8;
        }
        
        public StringTag(string str)
        {
            Str = str;
            Id = 8;
        }

        public StringTag()
        {
        }

        public override object Data()
        {
            return Str;
        }

        public override void HandleData(BeBinaryReader beBinaryReader)
        {
            _loadName(beBinaryReader);

            var length = beBinaryReader.ReadInt16();

            for (var i = 0; i < length; i++) Str += beBinaryReader.ReadChar();
        }

        public override void HandleDataList(BeBinaryReader beBinaryReader)
        {
            var length = beBinaryReader.ReadInt16();

            for (var i = 0; i < length; i++) Str += beBinaryReader.ReadChar();
        }

        public override void Save(BeBinaryWriter beBinaryWriter)
        {
            _saveId(beBinaryWriter);
            _saveName(beBinaryWriter);

            beBinaryWriter.Write((short) Str.Length);

            foreach (var ch in Str) beBinaryWriter.Write(ch);
        }

        public override void SaveList(BeBinaryWriter beBinaryWriter)
        {
            beBinaryWriter.Write((short) Str.Length);

            foreach (var ch in Str) beBinaryWriter.Write(ch);
        }

        public override string ToString()
        {
            return Name + ": \"" + Str + "\"";
        }

        public override string ToStringList()
        {
            return "\"" + Str + "\"";
        }

        protected bool Equals(StringTag other)
        {
            return string.Equals(Str, other.Str);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((StringTag) obj);
        }

        public override int GetHashCode()
        {
            return Str != null ? Str.GetHashCode() : 0;
        }
    }
}