using System;
using System.Collections.Generic;
using Be.IO;

namespace NBTParser.NBT.Tag
{
    public class ListTag : BaseTag
    {
        public byte TagID;
        public List<BaseTag> Tags;

        public ListTag(string name, BaseTag[] tags)
        {
            var tagsMatch = true;

            for (var i = 1; i < tags.Length; i++)
                if (tags[i].Id != tags[i - 1].Id)
                    tagsMatch = false;

            if (!tagsMatch)
                throw new ArrayTypeMismatchException();

            Name = name;
            Tags = new List<BaseTag>(tags);
            TagID = tags[0].Id;
            Id = 9;
        }

        public ListTag(BaseTag[] tags)
        {
            var tagsMatch = true;

            for (var i = 1; i < tags.Length; i++)
                if (tags[i].Id != tags[i - 1].Id)
                    tagsMatch = false;

            if (!tagsMatch)
                throw new ArrayTypeMismatchException();

            Tags = new List<BaseTag>(tags);
            TagID = tags[0].Id;
            Id = 9;
        }
        
        public ListTag()
        {
            Tags = new List<BaseTag>();
        }

        public override object Data()
        {
            return Tags;
        }

        public override void HandleData(BeBinaryReader beBinaryReader)
        {
            _loadName(beBinaryReader);

            TagID = beBinaryReader.ReadByte();
            Tags.Capacity = beBinaryReader.ReadInt32();

            for (var i = 0; i < Tags.Capacity; i++) Tags.Add(Parser.ParseTagList(beBinaryReader, TagID));
        }

        public override void HandleDataList(BeBinaryReader beBinaryReader)
        {
            TagID = beBinaryReader.ReadByte();
            Tags.Capacity = beBinaryReader.ReadInt32();

            for (var i = 0; i < Tags.Capacity; i++) Tags.Add(Parser.ParseTagList(beBinaryReader, TagID));
        }

        public override void Save(BeBinaryWriter beBinaryWriter)
        {
            _saveId(beBinaryWriter);
            _saveName(beBinaryWriter);

            beBinaryWriter.Write(TagID);
            beBinaryWriter.Write(Tags.Capacity);

            foreach (var tag in Tags) tag.SaveList(beBinaryWriter);
        }

        public override void SaveList(BeBinaryWriter beBinaryWriter)
        {
            beBinaryWriter.Write(TagID);
            beBinaryWriter.Write(Tags.Capacity);

            foreach (var tag in Tags) tag.SaveList(beBinaryWriter);
        }

        public override string ToString()
        {
            var tags = string.Empty;

            foreach (var tag in Tags) tags += tag.ToStringList() + ", ";

            tags = tags.TrimEnd().TrimEnd();

            return Name + ": " + "{" + tags + "}";
        }

        public override string ToStringList()
        {
            var tags = string.Empty;

            foreach (var tag in Tags) tags += tag.ToStringList() + ", ";

            tags = tags.TrimEnd().TrimEnd();

            return "{" + tags + "}";
        }

        protected bool Equals(ListTag other)
        {
            return Equals(Tags, other.Tags) && TagID == other.TagID;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ListTag) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Tags != null ? Tags.GetHashCode() : 0) * 397) ^ TagID.GetHashCode();
            }
        }
    }
}