﻿using Be.IO;

namespace NBTParser.NBT
{
    public abstract class BaseTag
    {
        public byte Id;

        public string Name;

        protected void _loadName(BeBinaryReader beBinaryReader)
        {
            int lenght = beBinaryReader.ReadInt16();

            for (var i = 0; i < lenght; i++) Name += beBinaryReader.ReadChar();
        }

        protected void _saveId(BeBinaryWriter beBinaryWriter)
        {
            beBinaryWriter.Write(Id);
        }

        protected void _saveName(BeBinaryWriter beBinaryWriter)
        {
            beBinaryWriter.Write((short) Name.Length);

            foreach (var ch in Name) beBinaryWriter.Write((byte) ch);
        }

        public abstract object Data();

        public abstract void HandleData(BeBinaryReader beBinaryReader);
        public abstract void HandleDataList(BeBinaryReader beBinaryReader);
        public abstract void Save(BeBinaryWriter beBinaryWriter);
        public abstract void SaveList(BeBinaryWriter beBinaryWriter);

        public abstract string ToStringList();
    }
}