﻿using System.Collections.Generic;
using Be.IO;

namespace NBTParser.NBT.Tag
{
    public class ByteArrayTag : BaseTag
    {
        public List<byte> Bytes;

        public ByteArrayTag(string name, IEnumerable<byte> bytes)
        {
            Name = name;
            Bytes = new List<byte>(bytes);
            Id = 7;
        }
        
        public ByteArrayTag(IEnumerable<byte> bytes)
        {
            Bytes = new List<byte>(bytes);
            Id = 7;
        }

        public ByteArrayTag()
        {
            Bytes = new List<byte>();
        }

        public override object Data()
        {
            return Bytes;
        }

        public override void HandleData(BeBinaryReader beBinaryReader)
        {
            _loadName(beBinaryReader);

            var size = beBinaryReader.ReadInt32();

            Bytes.Capacity = size;

            for (var i = 0; i < size; i++) Bytes.Add(beBinaryReader.ReadByte());
        }

        public override void HandleDataList(BeBinaryReader beBinaryReader)
        {
            var size = beBinaryReader.ReadInt32();

            Bytes.Capacity = size;

            for (var i = 0; i < size; i++) Bytes.Add(beBinaryReader.ReadByte());
        }

        public override void Save(BeBinaryWriter beBinaryWriter)
        {
            _saveId(beBinaryWriter);
            _saveName(beBinaryWriter);

            beBinaryWriter.Write(Bytes.Count);

            foreach (var b in Bytes) beBinaryWriter.Write(b);
        }

        public override void SaveList(BeBinaryWriter beBinaryWriter)
        {
            beBinaryWriter.Write(Bytes.Count);

            foreach (var b in Bytes) beBinaryWriter.Write(b);
        }

        public override string ToString()
        {
            var bytes = string.Empty;

            foreach (var b in Bytes) bytes += b + ", ";

            bytes = bytes.Remove(bytes.Length - 1);

            return Name + ": " + "{" + bytes + "}";
        }

        public override string ToStringList()
        {
            var bytes = string.Empty;

            foreach (var b in Bytes) bytes += b + ", ";

            bytes = bytes.TrimEnd().TrimEnd();

            return "{" + bytes + "}";
        }

        protected bool Equals(ByteArrayTag other)
        {
            return Equals(Bytes, other.Bytes);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ByteArrayTag) obj);
        }

        public override int GetHashCode()
        {
            return Bytes != null ? Bytes.GetHashCode() : 0;
        }
    }
}