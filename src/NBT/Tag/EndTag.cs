﻿using Be.IO;

namespace NBTParser.NBT.Tag
{
    public class EndTag : BaseTag
    {
        public EndTag()
        {
            Id = 0;
        }

        public override object Data()
        {
            return null;
        }

        public override void HandleData(BeBinaryReader beBinaryReader)
        {
        }

        public override void HandleDataList(BeBinaryReader beBinaryReader)
        {
        }

        public override void Save(BeBinaryWriter beBinaryWriter)
        {
            _saveId(beBinaryWriter);
        }

        public override void SaveList(BeBinaryWriter beBinaryWriter)
        {
        }

        public override string ToStringList()
        {
            return string.Empty;
        }

        protected bool Equals(EndTag other)
        {
            return true;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((EndTag) obj);
        }

        public override int GetHashCode()
        {
            return 0;
        }
    }
}