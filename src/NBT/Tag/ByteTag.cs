﻿using Be.IO;

namespace NBTParser.NBT.Tag
{
    public class ByteTag : BaseTag
    {
        public byte B;

        public ByteTag(string name, byte b)
        {
            Name = name;
            B = b;
            Id = 1;
        }
        
        public ByteTag(byte b)
        {
            B = b;
            Id = 1;
        }

        public ByteTag()
        {
        }

        public override object Data()
        {
            return B;
        }

        public override void Save(BeBinaryWriter beBinaryWriter)
        {
            _saveId(beBinaryWriter);
            _saveName(beBinaryWriter);

            beBinaryWriter.Write(B);
        }

        public override void SaveList(BeBinaryWriter beBinaryWriter)
        {
            beBinaryWriter.Write(B);
        }

        public override void HandleData(BeBinaryReader beBinaryReader)
        {
            _loadName(beBinaryReader);

            B = beBinaryReader.ReadByte();
        }

        public override void HandleDataList(BeBinaryReader beBinaryReader)
        {
            B = beBinaryReader.ReadByte();
        }

        public override string ToString()
        {
            return Name + ": " + B;
        }

        public override string ToStringList()
        {
            return B.ToString();
        }

        protected bool Equals(ByteTag other)
        {
            return B == other.B;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ByteTag) obj);
        }

        public override int GetHashCode()
        {
            return B.GetHashCode();
        }
    }
}