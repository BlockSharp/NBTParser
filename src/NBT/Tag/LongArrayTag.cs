﻿using System;
using System.Collections.Generic;
using Be.IO;

namespace NBTParser.NBT.Tag
{
    public class LongArrayTag : BaseTag
    {
        public List<Int64> Longs;

        public LongArrayTag(string name, IEnumerable<Int64> longs)
        {
            Name = name;
            Longs = new List<Int64>(longs);
            Id = 12;
        }
        
        public LongArrayTag(IEnumerable<Int64> longs)
        {
            Longs = new List<Int64>(longs);
            Id = 12;
        }

        public LongArrayTag()
        {
            Longs = new List<Int64>();
        }

        public override object Data()
        {
            return Longs;
        }

        public override void HandleData(BeBinaryReader beBinaryReader)
        {
            _loadName(beBinaryReader);

            var size = beBinaryReader.ReadInt32();

            Longs.Capacity = size;

            for (var i = 0; i < size; i++) Longs.Add(beBinaryReader.ReadInt64());
        }

        public override void HandleDataList(BeBinaryReader beBinaryReader)
        {
            var size = beBinaryReader.ReadInt32();

            Longs.Capacity = size;

            for (var i = 0; i < size; i++) Longs.Add(beBinaryReader.ReadInt64());
        }

        public override void Save(BeBinaryWriter beBinaryWriter)
        {
            _saveId(beBinaryWriter);
            _saveName(beBinaryWriter);

            beBinaryWriter.Write(Longs.Count);

            foreach (var i in Longs) beBinaryWriter.Write(i);
        }

        public override void SaveList(BeBinaryWriter beBinaryWriter)
        {
            beBinaryWriter.Write(Longs.Count);

            foreach (var i in Longs) beBinaryWriter.Write(i);
        }

        public override string ToString()
        {
            var longs = string.Empty;

            foreach (var l in Longs) longs += l + ", ";

            longs = longs.Remove(longs.Length - 1);

            return Name + ": " + "{" + longs + "}";
        }

        public override string ToStringList()
        {
            var longs = string.Empty;

            foreach (var l in Longs) longs += l + ", ";

            longs = longs.TrimEnd().TrimEnd();

            return "{" + longs + "}";
        }

        protected bool Equals(LongArrayTag other)
        {
            return Equals(Longs, other.Longs);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((LongArrayTag) obj);
        }

        public override int GetHashCode()
        {
            return Longs != null ? Longs.GetHashCode() : 0;
        }
    }
}