﻿using Be.IO;

namespace NBTParser.NBT.Tag
{
    public class ShortTag : BaseTag
    {
        public short S;

        public ShortTag(string name, short s)
        {
            Name = name;
            S = s;
            Id = 2;
        }
        
        public ShortTag(short s)
        {
            S = s;
            Id = 2;
        }

        public ShortTag()
        {
        }

        public override object Data()
        {
            return S;
        }

        public override void HandleData(BeBinaryReader beBinaryReader)
        {
            _loadName(beBinaryReader);

            S = beBinaryReader.ReadInt16();
        }

        public override void HandleDataList(BeBinaryReader beBinaryReader)
        {
            S = beBinaryReader.ReadInt16();
        }

        public override void Save(BeBinaryWriter beBinaryWriter)
        {
            _saveId(beBinaryWriter);
            _saveName(beBinaryWriter);

            beBinaryWriter.Write(S);
        }

        public override void SaveList(BeBinaryWriter beBinaryWriter)
        {
            beBinaryWriter.Write(S);
        }

        public override string ToString()
        {
            return Name + ": " + S;
        }

        public override string ToStringList()
        {
            return S.ToString();
        }

        protected bool Equals(ShortTag other)
        {
            return S == other.S;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ShortTag) obj);
        }

        public override int GetHashCode()
        {
            return S.GetHashCode();
        }
    }
}