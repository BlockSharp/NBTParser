﻿using Be.IO;

namespace NBTParser.NBT.Tag
{
    public class IntTag : BaseTag
    {
        public int I;

        public IntTag(string name, int i)
        {
            Name = name;
            I = i;
            Id = 3;
        }
        
        public IntTag(int i)
        {
            I = i;
            Id = 3;
        }

        public IntTag()
        {
        }

        public override object Data()
        {
            return I;
        }

        public override void HandleData(BeBinaryReader beBinaryReader)
        {
            _loadName(beBinaryReader);

            I = beBinaryReader.ReadInt32();
        }

        public override void HandleDataList(BeBinaryReader beBinaryReader)
        {
            I = beBinaryReader.ReadInt32();
        }

        public override void Save(BeBinaryWriter beBinaryWriter)
        {
            _saveId(beBinaryWriter);
            _saveName(beBinaryWriter);

            beBinaryWriter.Write(I);
        }

        public override void SaveList(BeBinaryWriter beBinaryWriter)
        {
            beBinaryWriter.Write(I);
        }

        public override string ToString()
        {
            return Name + ": " + I;
        }

        public override string ToStringList()
        {
            return I.ToString();
        }

        protected bool Equals(IntTag other)
        {
            return I == other.I;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((IntTag) obj);
        }

        public override int GetHashCode()
        {
            return I;
        }
    }
}