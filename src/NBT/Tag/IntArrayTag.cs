﻿using System;
using System.Collections.Generic;
using Be.IO;

namespace NBTParser.NBT.Tag
{
    public class IntArrayTag : BaseTag
    {
        public List<Int32> Ints;

        public IntArrayTag(string name, IEnumerable<Int32> ints)
        {
            Name = name;
            Ints = new List<Int32>(ints);
            Id = 11;
        }
        
        public IntArrayTag(IEnumerable<Int32> ints)
        {
            Ints = new List<Int32>(ints);
            Id = 11;
        }

        public IntArrayTag()
        {
            Ints = new List<Int32>();
        }

        public override object Data()
        {
            return Ints;
        }

        public override void HandleData(BeBinaryReader beBinaryReader)
        {
            _loadName(beBinaryReader);

            var size = beBinaryReader.ReadInt32();

            Ints.Capacity = size;

            for (var i = 0; i < size; i++) Ints.Add(beBinaryReader.ReadInt32());
        }

        public override void HandleDataList(BeBinaryReader beBinaryReader)
        {
            var size = beBinaryReader.ReadInt32();

            Ints.Capacity = size;

            for (var i = 0; i < size; i++) Ints.Add(beBinaryReader.ReadInt32());
        }

        public override void Save(BeBinaryWriter beBinaryWriter)
        {
            _saveId(beBinaryWriter);
            _saveName(beBinaryWriter);

            beBinaryWriter.Write(Ints.Count);

            foreach (var i in Ints) beBinaryWriter.Write(i);
        }

        public override void SaveList(BeBinaryWriter beBinaryWriter)
        {
            beBinaryWriter.Write(Ints.Count);

            foreach (var i in Ints) beBinaryWriter.Write(i);
        }

        public override string ToString()
        {
            var ints = string.Empty;

            foreach (var i in Ints) ints += i + ", ";

            ints = ints.Remove(ints.Length - 1);

            return Name + ": " + "{" + ints + "}";
        }

        public override string ToStringList()
        {
            var ints = string.Empty;

            foreach (var i in Ints) ints += i + ", ";

            ints = ints.TrimEnd().TrimEnd();

            return "{" + ints + "}";
        }

        protected bool Equals(IntArrayTag other)
        {
            return Equals(Ints, other.Ints);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((IntArrayTag) obj);
        }

        public override int GetHashCode()
        {
            return Ints != null ? Ints.GetHashCode() : 0;
        }
    }
}